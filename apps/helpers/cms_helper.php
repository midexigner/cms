<?php 
function image_path(){
  return base_url().'upload/';
}
function btn_edit($uri){
	$atts = array('class'  => 'btn btn-xs btn-default');
	return anchor($uri, '<i class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Edit Items"></i>',$atts);
}

function btn_delete($uri){
	$atts = array('class'  => 'btn btn-xs btn-default','onclick'=>"return confirm('You are about to delete a record. This cannot be undone. Are you sure?')");
	return anchor($uri, '<i class="glyphicon glyphicon-remove" data-toggle="tooltip" data-placement="top" title="Delete items"></i>',$atts);
}
function btn_restore($uri){
  $atts = array('class'  => 'btn btn-xs btn-default','onclick'=>"return confirm('You are about to restore a record. Are you sure?')");
  return anchor($uri, '<i class="glyphicon glyphicon-refresh" data-toggle="tooltip" data-placement="top" title="Restore items"></i>',$atts);
}


function pr($debug){echo "<pre>";print_r($debug);echo "</pre>";}


function vd($debug){echo "<pre>";var_dump($debug);echo "</pre>";}


function pretty_date($date){return date("M d, Y h:i A",strtotime($date));}

function getIp() {$ip = $_SERVER['REMOTE_ADDR'];if (!empty($_SERVER['HTTP_CLIENT_IP'])) {$ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];}return $ip;}


function time_left($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}


function money($number){
return 'Rs '.number_format($number,2);
}
function sizesToArray($string){
  $sizesArray = explode(',',$string);
  $returnArray = array();
  foreach ($sizesArray as $size) {
    $s = explode(':',$size);
    $returnArray[] = array('size'=> $s[0] , 'quantity' => $s[1], 'color' => $s[2]);
  }
  return $returnArray;
}

function sizesToString($sizes){
  $sizeString = '';
  foreach ($sizes as $size) {
    $sizeString .= $size['size'].":".$size['quantity'].':'.$size['color'].',';
  }
  $trimmed = rtrim($sizeString,',');
  return $trimmed;
}

 ?>
