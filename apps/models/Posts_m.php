<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Posts_M extends MI_Model
{
	
	
	function __construct ()
	{
		parent::__construct();
	}

	public function get_posts($slug = false){
		if($slug === false){
			$this->db->order_by('p.id','DESC');
			$this->db->join('categories c','c.id = p.categories');
			$query = $this->db->get_where('posts p',array('type'=>1));

			return $query->result_array();
		}
		$query = $this->db->get_where('posts',array('slug'=>$slug,'type'=>1));
		return $query->result_array();
	}

}