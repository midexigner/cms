<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Post_M extends MI_Model
{
	protected $_table_name = 'posts';
	protected $_order_by = 'title';
	public $rules = array(
		'title' => array(
			'field' => 'post_title',
			'label' => 'Title',
			'rules' => 'trim|required'
		)
	);

	function __construct ()
	{
		parent::__construct();
	}
public function get_posts($slug = false){
		if($slug === false){
			$this->db->order_by('p.id','DESC');
			$this->db->join('taxonomy t','t.id = p.taxonomy_id');
			$query = $this->db->get_where('posts p',array('post_type'=>'post'));

//echo $this->db->last_query();
			return $query->result_array();
		}
		$query = $this->db->get_where('posts',array('post_name'=>$slug));

		return $query->result_array();
	}

public function getAllRecord($id)
	{
		 $query = $query = $this->db->get_where('posts', array('post_name' => $id));
		 if($query->num_rows() > 0){
			return $query->row();
		}
	}
	public function create_post($post_image){
		$slug = url_title($this->input->post('slug'));
		$date = date("Y-m-d H:i:s");
		$data = array(
			'title' => $this->input->post('title'),
			'slug'  => $slug,
			'body'  => $this->input->post('body'),
			'post_image' =>$post_image,
			'categories'  => $this->input->post('categories'),
			'type'  => 1,
			'created_at' => $date
		);



		return $this->db->insert('posts',$data);
	}
public function update_post(){
$id = $this->input->post('id');
$slug = url_title($this->input->post('slug'));
$data = array(
        'title' => $this->input->post('title'),
        'slug' => $slug,
        'body' => $this->input->post('body'),
        'categories'  => $this->input->post('categories'),
        );
$this->db->where('id',$id);
return $this->db->update('posts',$data);
}

function getCategoriesByParentId($category_id) {

    $data = $this->db->select('*')->from('taxonomy')->WHERE('parent',$category_id,'taxonomy_type ', 'category')->get()->result_array();
    for($i=0;$i<count($data);$i++)
    {
        if($this->getCategoriesByParentId($data[$i]['id']))
        {
            $data[$i]['child']=$this->getCategoriesByParentId($data[$i]['id']);
        }
    }
    return $data;
}

public function get_categories(){
$this->db->order_by('name');

$this->db->where('taxonomy_type ', 'category');
$query = $this->db->get('taxonomy');
return $query->result_array();
}


}
