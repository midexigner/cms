<?php
class Admin_Controller extends MI_Controller
{

	function __construct ()
	{
		parent::__construct();
		$this->data['cms_title'] = 'CMS 1.0';
		$this->data['cms_url'] = "http://cloud-innovator.com";
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('upload');
		$this->load->library('Custom_Upload');
		$this->load->library('session');
		$this->load->library('email');
		$this->load->library('parser');
		$this->load->library('table');
		$this->load->helper('security');
		$this->load->helper('text');
		$this->load->model('users_m');
		$this->load->model('post_m');

	}
}
