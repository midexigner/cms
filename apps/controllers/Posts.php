<?php 
class Posts extends Frontend_Controller {
	public function index(){
	$this->data['title']= 'Latest Post'; 
	$this->load->view('template/header');
	$this->data['posts'] = $this->posts_m->get_posts();
	$this->load->view('posts/index',$this->data);
	$this->load->view('template/footer');

}

public function view($slug = null){
	
	$data['post'] = $this->posts_m->get_posts($slug);
	if(empty($data['post'])){
		show_404();
	}
	#$data['title'] = $data['post']['title']; 
	$this->load->view('template/header');
	$this->load->view('posts/view',$data);
	$this->load->view('template/footer');
}


}
 ?>