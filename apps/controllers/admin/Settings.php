<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends Admin_Controller {


public function __construct()
	{

        parent::__construct();
        if ($_SESSION['user_logged'] == FALSE) {
            $this->session->set_flashdata("error", "<div class='alert alert-dismissible alert-danger'>You must be logged in to access that page.</div>");
            redirect("admin/login");
        }
	}
	public function index(){
		$this->load->view('admin/common/head',$this->data);
		$this->load->view('admin/common/header');
		$this->load->view('admin/common/sidebar');
		#$this->data['posts'] = $this->post_m->get_posts();
		$this->load->view('admin/settings/index',$this->data);
		$this->load->view('admin/common/footer');
		$this->load->view('admin/common/foot');

	}



}
