<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends Admin_Controller {
public function __construct()
	{
		parent::__construct();


	}

public function index(){
     if ($_SESSION['user_logged'] == FALSE) {
            $this->session->set_flashdata("error", "<div class='alert alert-dismissible alert-danger'>You must be logged in to access that page.</div>");
            redirect("admin/login");
        }else{
    if ($this->session->userdata('role') == 'editor') {
            $this->session->set_flashdata("response", "<div class='alert alert-dismissible alert-danger'>You do not have permission to access that page!! </div>");
            redirect("admin/dashboard");
        }else{
        $this->parser->parse('admin/common/head', $this->data);
		$this->load->view('admin/common/header');
		$this->load->view('admin/common/sidebar');
		// Fetch all users
        $this->data['users'] = $this->users_m->get_all_users();
		// Load view
        $this->load->view('admin/users/users', $this->data);
		$this->load->view('admin/common/footer');
		$this->load->view('admin/common/foot');

        }
    }
}

public function create()
{
     if ($_SESSION['user_logged'] == FALSE) {
            $this->session->set_flashdata("error", "<div class='alert alert-dismissible alert-danger'>You must be logged in to access that page.</div>");
            redirect("admin/login");
        }else{
 if ($this->session->userdata('permissions') == 'role') {
            $this->session->set_flashdata("response", "<div class='alert alert-dismissible alert-danger'>You do not have permission to access that page!! </div>");
            redirect("admin/dashboard");
        }else{
        $this->form_validation->set_rules('first_name', 'First Name', 'required|min_length[3]|xss_clean');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required|min_length[3]|xss_clean');
        $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|is_unique[users.email]|xss_clean');
        $this->form_validation->set_rules('password',"Password",'required|min_length[3]|max_length[6]|trim|xss_clean');
        $this->form_validation->set_rules('confirm',"Password Confirm",'required|trim|matches[password]|xss_clean');
        $this->form_validation->set_rules('role', 'Role', 'required|xss_clean');
        $this->form_validation->set_error_delimiters('<div class="text-danger alert-dismissible">', '</div>');
        $this->form_validation->set_message('is_unique',"That Email already exists");
        if ($this->form_validation->run())
        {

            $data = array(
                        'first_name'  => $this->input->post('first_name'),
                        'last_name'  => $this->input->post('last_name'),
                        'email'  => $this->input->post('email'),
                        'status'  => $this->input->post('status'),
                        'password'  => do_hash($this->input->post('password')),
                        'role'  => $this->input->post('role'),

                        );
            #$data = $this->input->post();
            if($this->users_m->userregister($data)){
                $this->session->set_flashdata('response',"<div class='alert alert-dismissible alert-success'>Your account has been registered. You can login now</div>");
            }else{
                $this->session->set_flashdata('response',"<div class='alert alert-dismissible alert-danger'>Your account has been not registered. Please try again</div>");
            }
            return redirect('admin/users/users');
        }
        else
        {
        $this->load->view('admin/common/head',$this->data);
        $this->load->view('admin/common/header');
        $this->load->view('admin/common/sidebar');
        $this->load->view('admin/users/create');
        $this->load->view('admin/common/footer');
        $this->load->view('admin/common/foot');
        }

             }
    }
}







    public function change()
{
     if ($_SESSION['user_logged'] == FALSE) {
            $this->session->set_flashdata("error", "<div class='alert alert-dismissible alert-danger'>You must be logged in to access that page.</div>");
            redirect("admin/login");
        }else{
        $this->load->view('admin/common/head',$this->data);
        $this->load->view('admin/common/header');
        $this->load->view('admin/common/sidebar');
        $this->load->view('aadmin/users/user_change');
        $this->load->view('admin/common/footer');
        $this->load->view('admin/common/foot');
    }

}

public function delete($id)
    {
         if ($_SESSION['user_logged'] == FALSE) {
            $this->session->set_flashdata("error", "<div class='alert alert-dismissible alert-danger'>You must be logged in to access that page.</div>");
            redirect("admin/login");
        }else{
        $this->users_m->delete($id);
        redirect('admin/users/');
    }

    }




/* login page view */
public function login()
    {

        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
        if ($this->form_validation->run() == TRUE) {
             $email = $_POST['email'];
            $password =$_POST['password'];
            $password_hashed = do_hash($password);

             //check user in database
            $this->db->select('id,first_name,last_name,email,password,status,role');
            $this->db->from('users');
            $this->db->where(array('email' => $email, 'password' => $password_hashed));
            $query = $this->db->get();
            $user = $query->row();
	    $userCount = $query->num_rows();

	if($userCount == 1){

if($user->status == 'active'){
            if (isset($user->email)) {

                // temporary message
                $this->session->set_flashdata("response", "<div class='alert alert-dismissible alert-success'>You are now logged in</div>");

                // set session variables
                $data = array(
                    'user_logged' => TRUE,
                    'id' => $user->id,
            'full_name' => $user->first_name.' '.$user->last_name,
            'role' =>$user->role,
                );
                $this->session->set_userdata($data);
               $id = $this->session->userdata('id');
              $this->users_m->last_login($id);
                // redirect to profile page
                redirect("admin/dashboard", "refresh");
            } else {
                $this->session->set_flashdata("error", "<div class='alert alert-dismissible alert-danger'>NO such account exists in database</div>");
                redirect("admin/login", "refresh");
            }
}else{
$this->session->set_flashdata("error", "<div class='alert alert-dismissible alert-danger'>User will be inactive call to administrator</div>");
}


}
else
{
$this->session->set_flashdata("error", "<div class='alert alert-dismissible alert-danger'>NO such account exists in database</div>");
                redirect("admin/login", "refresh");
}
// if user exists

        }
        $this->load->view('admin/login',$this->data);
    }



public function logout(){
        $this->session->sess_destroy();
        unset($_SESSION);
        redirect("admin/login", "refresh");
    }


    public function forget(){
        $this->load->view('admin/forget',$this->data);
    }


}




 ?>
