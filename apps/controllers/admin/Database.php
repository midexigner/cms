<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Database extends Admin_Controller {

public function __construct()
	{
		parent::__construct();
if ($_SESSION['user_logged'] == FALSE) {
            $this->session->set_flashdata("error", "<div class='alert alert-dismissible alert-danger'>You must be logged in to access that page.</div>");
            redirect("admin/login");
        }

		$this->load->dbutil();
		$this->load->helper('file');
		$this->load->helper('download');
	#$this->load->helper('zip');
     }

	public function index()

	{
		if ($this->session->userdata('permissions') == 'editor') {
            $this->session->set_flashdata("response", "<div class='alert alert-dismissible alert-danger'>You do not have permission to access that page!! </div>");
            redirect("admin/dashboard");
        }else{

    $DBUSER=$this->db->username;
    $DBPASSWD=$this->db->password;
    $DATABASE=$this->db->database;

    $filename = $DATABASE . "-" . date("Y-m-d_H-i-s") . ".sql.zip";
    $mime = "application/x-gzip";

    header( "Content-Type: " . $mime );
    header( 'Content-Disposition: attachment; filename="' . $filename . '"' );

    // $cmd = "mysqldump -u $DBUSER --password=$DBPASSWD $DATABASE | gzip --best";
    $cmd = "mysqldump -u $DBUSER --password=$DBPASSWD --no-create-info --complete-insert $DATABASE | gzip --best";

    passthru( $cmd );

    exit(0);

}


}
public function db_backup()
{
if ($this->session->userdata('permissions') == 'editor') {
            $this->session->set_flashdata("response", "<div class='alert alert-dismissible alert-danger'>You do not have permission to access that page!! </div>");
            redirect("admin/dashboard");
        }else{
        $db_format = array(
                'format'      => 'zip',
                'filename'    => 'my_db_backup.sql'
              );


        $backup =& $this->dbutil->backup($db_format );

        $db_name = 'backup-on-'. date("Y-m-d-H-i-s") .'.zip';
        $save = 'db/'.$db_name;
        write_file($save, $backup);
        $this->load->helper('download');
		force_download($db_name, $backup);
}


}


public function restore(){
if ($this->session->userdata('permissions') == 'editor') {
            $this->session->set_flashdata("response", "<div class='alert alert-dismissible alert-danger'>You do not have permission to access that page!! </div>");
            redirect("admin/dashboard");
        }else{

	$this->load->view('admin/common/head',$this->data);
		$this->load->view('admin/common/header');
		$this->load->view('admin/common/sidebar');
$this->load->view('admin/database');
$this->load->view('admin/common/footer');
		$this->load->view('admin/common/foot');

		}

}






}
