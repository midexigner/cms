<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Post extends Admin_Controller {


public function __construct()
	{
		
        parent::__construct();
        if ($_SESSION['user_logged'] == FALSE) {
            $this->session->set_flashdata("error", "<div class='alert alert-dismissible alert-danger'>You must be logged in to access that page.</div>");
            redirect("admin/login");
        }
	}
	public function index(){
		
		$this->load->view('admin/common/head',$this->data);
		$this->load->view('admin/common/header');
		$this->load->view('admin/common/sidebar');
		$this->data['posts'] = $this->post_m->get_posts();
		$this->load->view('admin/post/index',$this->data);
		$this->load->view('admin/common/footer');
		$this->load->view('admin/common/foot');

	}

	public function create(){
		$data['title'] = 'Add New Post';
		$data['categories'] = $this->post_m->get_categories();
		$this->form_validation->set_rules('title','Title','required');
		$this->form_validation->set_rules('body','Body','required');
		$this->form_validation->set_error_delimiters('<div class="text-danger alert-dismissible">', '</div>');
		if($this->form_validation->run() === TRUE){
			// Upload Image
			$config['upload_path'] = './upload/posts';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size'] = '2048';
			$config['max_width'] = '500';
			$config['max_height'] = '500';

			$this->load->library('upload',$config);

			if(!$this->upload->do_upload('userfile')){
				$errors = array('error' => $this->upload->display_errors());
				$post_image = 'noimage.jpg';

			}else{
			#$data = array('upload_data'=> $this->upload->data());
			//$post_image = $_FILES['userfile']['name'];
			$post_image = $this->upload->data($config);
			}


			$this->post_m->create_post($post_image);
			
			 $this->session->set_flashdata("response", "<div class='alert alert-dismissible alert-success'>Post Has been Created. </div>");
			redirect('admin/post');

		}else{
			$this->load->view('admin/common/head',$this->data);
		$this->load->view('admin/common/header');
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/post/create',$data);
		$this->load->view('admin/common/footer');
		$this->load->view('admin/common/foot');
		}

	}

	public function delete($id)
    {
        $this->post_m->delete($id);
        redirect('admin/post/');

    }


    public function edit($slug)
    {
    	
	$data['post'] = $this->post_m->getAllRecord($slug);
	$data['categories'] = $this->post_m->get_categories();
    if(empty($data['post'])){
    		show_404();
    	}
        
		$this->load->view('admin/common/head',$this->data);
        $this->load->view('admin/common/header');
        $this->load->view('admin/common/sidebar');
        $this->load->view('admin/post/edit',$data);
         $this->load->view('admin/common/footer');
        $this->load->view('admin/common/foot');
		

    }

    public function update(){
    	if($this->post_m->update_post()){
    	$this->session->set_flashdata("response", "<div class='alert alert-dismissible alert-success'>Post Has been Created. </div>");
			redirect('admin/post');
    }

}



}