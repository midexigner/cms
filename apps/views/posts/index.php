<div class="container">
	<div class="row">

<h3><?php echo $title; ?></h3>

<div class="row">
<div class="col-md-9">
	<?php foreach ($posts as $post) : ?>
		<h3><?php echo $post['title']; ?></h3>
	<?php if($post['featured'] !=''){ ?>
	<div class="col-md-3">
		<img src="<?php echo site_url('upload/posts').'/'.$post['featured']; ?>" class="img-responsive" alt="<?php echo $post['title']; ?>">
	</div>
<?php } ?>
	<div class="col-md-<?php echo (($post['featured']!=''?'9':'12')); ?> ">
		<small class="post-date">Posted on: <?php echo $post['created_at']; ?> in  <strong><?php echo $post['name'] ?></strong></small>
<?php if(strpos($post['body'],2)) ?>
<?php echo word_limiter($post['body'],60); ?>
<p><a href="<?php echo site_url('posts/'.$post['slug']); ?>" class="btn-link">Read More</a></p>
	</div>
	<?php endforeach; ?>
</div>
<div class="col-md-3 blog-sidebar">
	<div class="sidebar-module sidebar-module-inset">
	            <h4>About</h4>
	            <p>Etiam porta <em>sem malesuada magna</em> mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.</p>
	          </div>
	          <div class="sidebar-module">
	            <h4>Archives</h4>
	            <ol class="list-unstyled">
	              <li><a href="#">March 2014</a></li>
	              <li><a href="#">February 2014</a></li>
	              <li><a href="#">January 2014</a></li>
	              <li><a href="#">December 2013</a></li>
	              <li><a href="#">November 2013</a></li>
	              <li><a href="#">October 2013</a></li>
	              <li><a href="#">September 2013</a></li>
	              <li><a href="#">August 2013</a></li>
	              <li><a href="#">July 2013</a></li>
	              <li><a href="#">June 2013</a></li>
	              <li><a href="#">May 2013</a></li>
	              <li><a href="#">April 2013</a></li>
	            </ol>
	          </div>
	          <div class="sidebar-module">
	            <h4>Elsewhere</h4>
	            <ol class="list-unstyled">
	              <li><a href="#">GitHub</a></li>
	              <li><a href="#">Twitter</a></li>
	              <li><a href="#">Facebook</a></li>
	            </ol>
</div>
</div>


	</div>
</div>
