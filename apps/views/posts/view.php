<div class="container">
	<div class="row">
<?php foreach ($post as $posts) : ?>
<h3><?php echo $posts['title']; ?></h3>
<small class="post-date">Posted on: <?php echo $posts['created_at']; ?></small>
<div class="post-body"><?php echo $posts['body']; ?></div>
<?php endforeach; ?>

	</div>
</div>