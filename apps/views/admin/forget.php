<?php echo doctype('html5'); ?>
<html>
<head>
	<title><?php echo $meta_title ?></title>
	<base href = "<?php echo base_url(); ?>assets/admin/" />
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/login.css">
</head>
<body>
<div class="login-page">
  <?php if($error = $this->session->flashdata('error')): ?>
  <?php echo $error; ?>

    <?php endif; ?>
  <div class="form">
<?php echo validation_errors(); ?>
<?php 
$attributes = array('class' => 'login-form', 'id' => 'myform');
echo form_open('admin/forget',$attributes); ?>
<?php $attributes = array(
        'class' => 'sr-only',
);
echo form_label('Email', 'email', $attributes); ?>

  <?php
$data = array(
        'name'          => 'email',
        'id'            => 'email',
        'placeholder'     => 'Email'
);
 echo form_input($data,set_value('email')); ?>
<?php $attributes = array(
        'class' => 'sr-only',
);
echo form_label('Password', 'password', $attributes); ?>

  <?php
$data = array(
        'name'          => 'password',
        'id'            => 'password',
        'placeholder'     => 'New Password'
);
 echo form_password($data,set_value('')); ?>
 <?php echo 
form_submit(['value'=>'Forget Password']); ?>
      <p class="message">Back to <?php echo anchor(site_url('admin/'), 'Login'); ?></p>
    <?php echo form_close();?>
  </div>
</div>


<script type="text/javascript" src="js/jquery-1.12.0.min.js"></script>
<script>
  $(function () {
$('.alert-dismissible').delay(1000).slideUp(1000)
  });
</script>
</body>
</html>