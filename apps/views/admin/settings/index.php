<div class="col-md-10 content">
          <div class="row">
  <div class="col-md-12">
    <?php if($error = $this->session->flashdata('response')): ?>
  <?php echo $error; ?>
    <?php endif; ?>
  </div>

</div>
            <div class="row">
             <div class="col-md-12">

                <div class="panel panel-default">

                <div class="panel-heading text-center">

       	  <div class="row text-left">
<div class="col-md-6"><h4>Settings</h4></div>
    </div>

	</div>
                 <div class="panel-body">

                   <?php echo validation_errors(); ?>
                   <?php
                   $attributes = array('class' => 'form-settings', 'id' => 'myform');
                   echo form_open_multipart('admin/settings',$attributes); ?>
<div class="row">
  <div class=" col-md-6">


  <div class="form-group">
    <?php $attributes = array('class' => '',);
  echo form_label('Logo ', 'logo', $attributes); ?>
  <?php $data = array('name'=>'logo','id'=>'logo','class'=>'form-control');
   echo form_upload($data,set_value('title')); ?>
  </div>
  <div class="form-group">
    <?php $attributes = array('class' => '',);
  echo form_label('Favicon icon ', 'favicon', $attributes); ?>
  <?php $data = array('name'=>'favicon','id'=>'favicon','class'=>'form-control');
   echo form_upload($data,set_value('title')); ?>
  </div>
  <div class="form-group">
      <?php echo form_label('Meta Keyword', 'meta_keyword'); ?>
    <?php
  $data = array('name' =>'meta_keyword','id'=> 'meta_keyword','class'=> 'form-control','rows'=>'3');
   echo form_textarea($data,set_value('meta_keyword')); ?>

  </div>
  <div class="form-group">
      <?php echo form_label('Meta Description', 'meta_description'); ?>
    <?php
  $data = array('name' =>'meta_description','id'=> 'meta_description','class'=> 'form-control','rows'=>'3');
   echo form_textarea($data,set_value('meta_description')); ?>

  </div>
  <div class="form-group">
    <?php $attributes = array('class' => '',);
  echo form_label('Company Name: ', 'cname', $attributes); ?>
  <?php $data = array('name'=>'cname','id'=>'cname','class'=>'form-control','maxlength'=> '60');
   echo form_input($data,set_value('cname')); ?>
  </div>

  <div class="form-group">
      <?php echo form_label('Google Analytics Script', 'googlecode'); ?>
    <?php
  $data = array('name' =>'googlecode','id'=> 'googlecode','class'=> 'form-control','rows'=>'6');
   echo form_textarea($data,set_value('googlecode')); ?>

  </div>

  <div class="form-group" style="margin-top:25px;">
  <?php
$atts = array('class'  => 'btn btn-default');
echo  anchor('admin/settings','Cancel' ,$atts) ; ?>
<?php echo form_submit(['value'=>'Update','class'=>'btn btn-success']); ?>
</div>

    </div>
</div>
<?php echo form_close();?>
                 </div>

                </div>
                </div>

            </div>
