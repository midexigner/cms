<?php echo doctype('html5'); ?>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $cms_title ?> | Log in</title>
	<base href = "<?php echo base_url(); ?>assets/admin/" />
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="fonts-icons/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="fonts-icons/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">

<div class="login-box">
	<div class="login-logo">
		<a href="<?php echo base_url(); ?>"><b><?php echo $cms_title ?></a>
	</div>
	<!-- /.login-logo -->

  <div class="login-box-body">
		<?php if($error = $this->session->flashdata('error')): ?>
		<?php echo $error; ?>

			<?php endif; ?>
		  <p class="login-box-msg">Sign in to start your session</p>
<?php echo validation_errors(); ?>
<?php
$attributes = array('class' => 'login-form', 'id' => 'myform');
echo form_open('admin/login',$attributes); ?>
<div class="form-group has-feedback">
	<input type="email" class="form-control" id="email" name="email" placeholder="Email">
	<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
</div>

<div class="form-group has-feedback">
	<input type="password" name="password" id="password" class="form-control" placeholder="Password">
	<span class="glyphicon glyphicon-lock form-control-feedback"></span>
</div>

<div class="row">
	<div class="col-xs-8">
		<div class="checkbox icheck">

		</div>
		<p class="message">Not Know? <?php echo anchor(site_url('admin/forget'), 'Forget Password'); ?></p>
	</div>
	<!-- /.col -->
	<div class="col-xs-4">
		<button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>

	</div>
	<!-- /.col -->
</div>
    <?php echo form_close();?>
  </div>
	<!-- /.login-box-body -->
</div>

<!-- /.login-box -->


<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>

<script>
  $(function () {
$('.alert-dismissible').delay(1000).slideUp(1000)
  });
</script>
</body>
</html>
