</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- dataTables  -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<!-- dataTables bootstrap  -->
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- Tinymce -->
<script src="js/tinymce/tinymce.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- cutom -->
<script src="js/main.js"></script>
<!-- Tinymce Configure & slug configure -->

<script>
/*  $(function () {
    $('.table-post').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
*/
	jQuery(function(){

"use strict";
 $('#title').keyup(function(e){
      var str = $(this).val();
      var trims = $.trim(str);
      var slug = trims.replace(/[^a-z0-9]/gi,'-').replace(/-+/g,'-').replace(/^-|-^$/g,'');
	  $("#slug").val(slug.toLowerCase());
 });

 if (typeof(base_url) == "undefined") {
                 var base_url = location.protocol + '//' + location.host + '/';
             }
       tinymce.init({
    selector: '.editor',
    height: 200,
    menubar: false,
    branding: false,
    theme: 'modern',
      convert_urls:true,
    relative_urls:false,
 remove_script_host:false,
    mobile: { theme: 'mobile' },
    plugins: [
      'advlist autolink lists link image charmap print preview anchor fontawesome',
      'searchreplace visualblocks code fullscreen noneditable',
      'insertdatetime media table contextmenu paste code bootstrap'
    ],
    // bootstrap
    toolbar: ' undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code fontawesome | preview',
    // enable title field in the Image dialog
   image_title: true,
   // enable automatic uploads of images represented by blob or data URIs
   automatic_uploads: true,
    // add custom filepicker only to Image dialog
   file_picker_types: 'image',
    file_picker_callback: function(cb, value, meta) {
     var input = document.createElement('input');
     input.setAttribute('type', 'file');
     input.setAttribute('accept', 'image/*');

     input.onchange = function() {
       var file = this.files[0];
       var reader = new FileReader();

       reader.onload = function () {
         var id = 'blobid' + (new Date()).getTime();
         var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
         var base64 = reader.result.split(',')[1];
         var blobInfo = blobCache.create(id, file, base64);
         blobCache.add(blobInfo);

         // call the callback and populate the Title field with the file name
         cb(blobInfo.blobUri(), { title: file.name });
       };
       reader.readAsDataURL(file);
     };

     input.click();
   },

   content_css:  base_url + 'tinymce/plugins/fontawesome/css/font-awesome.min.css',
   noneditable_noneditable_class: 'fa',
   extended_valid_elements: 'span[*]',
   bootstrapConfig: {
                     'bootstrapCssPath': base_url + 'tinymce/plugins/bootstrap/css/bootstrap.min.css',
                     'imagesPath': '/img/',
                     'bootstrapIconFont': 'materialdesign',
                                         'allowSnippetManagement': true
                     // 'imagesPath': '/demo/img/' // localhost
                 },

  });

});


</script>
</body>
</html>
