<!-- =============================================== -->

<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php echo $this->session->userdata('full_name');  ?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">MAIN NAVIGATION</li>
      <li  class="treeview">
          <a href="<?php echo site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
        </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-pushpin"></i> <span>Posts</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo site_url('admin/post') ?>"><i class="fa fa-circle-o"></i>All Post </a></li>
          <li><a href="<?php echo site_url('admin/post/create') ?>"><i class="fa fa-circle-o"></i>Add New</a></li>
          <li><a href="<?php echo site_url('admin/taxonomy/category') ?>"><i class="fa fa-circle-o"></i>Categories </a></li>
            <li><a href="<?php echo site_url('admin/taxonomy/tag') ?>"><i class="fa fa-circle-o"></i>Tags </a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-file"></i> <span>Pages</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><?php echo anchor(site_url().'admin/post/post_type/page', '<i class="fa fa-circle-o"></i> List '); ?></li>
          <li><?php echo anchor(site_url('admin/post/post_type/create'),'<i class="fa fa-circle-o"></i> Add New'); ?></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-user"></i> <span>Users</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo site_url('admin/users'); ?>"><i class="fa fa-circle-o"></i> All User</a>
          <li><a href="<?php echo site_url('admin/users/create'); ?>"><i class="fa fa-circle-o"></i> Add New</a>
          <li><a href="<?php echo site_url('admin/users/profile'); ?>"><i class="fa fa-circle-o"></i> Your Profile</a>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-cog"></i> <span>Setting</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo site_url('admin/settings'); ?>"><i class="fa fa-circle-o"></i> General</a></li>
          <li><a href="<?php echo site_url('admin/settings/options-reading'); ?>"><i class="fa fa-circle-o"></i> Reading</a></li>
          <?php if($this->session->userdata('role') == 'admin') { ?>
          <li><a href="<?php echo site_url('admin/database/db_backup'); ?>"><i class="fa fa-circle-o"></i> Backup</a></li>
          <li><a href="<?php echo site_url('admin/database/restore'); ?>"><i class="fa fa-circle-o"></i> Restore</a></li>
        <?php } ?>
        </ul>
      </li>



      <!-- <li class="treeview">
        <a href="#">
          <i class="fa fa-share"></i> <span>Multilevel</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
          <li>
            <a href="#"><i class="fa fa-circle-o"></i> Level One
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
              <li>
                <a href="#"><i class="fa fa-circle-o"></i> Level Two
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                  <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                </ul>
              </li>
            </ul>
          </li>
          <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
        </ul>
      </li> -->

    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
