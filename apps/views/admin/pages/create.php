<div class="col-md-10 content">
<div class="row">
 <div class="col-md-12">
<div class="panel panel-default">
     <div class="panel-heading text-center"><h4>Add  New Post</h4></div>

     <div class="panel-body">
<?php echo validation_errors(); ?>
<?php
$attributes = array('class' => 'form-', 'id' => 'myform');
echo form_open_multipart('admin/pages/create',$attributes); ?>
<div class="row">
<div class="form-group col-md-6">
  <?php $attributes = array(
        'class' => '',
);
echo form_label('Title: ', 'title', $attributes); ?>

  <?php
$data = array(
        'name'          => 'title',
        'id'            => 'title',
        'class'            => 'form-control',
        'maxlength'     => '60'
);
 echo form_input($data,set_value('title')); ?>

</div>
<div class="form-group col-md-6">
  <?php $attributes = array(
        'class' => '',
);
echo form_label('Slug:', 'slug', $attributes); ?>

  <?php
$data = array(
        'name'          => 'slug',
        'id'            => 'slug',
        'class'            => 'form-control',
        'readonly'     => 'readonly'
);
 echo form_input($data,set_value('slug')); ?>

</div>
</div>
<div class="row">
<div class="form-group col-md-6">
    <?php echo form_label('Content', 'body'); ?>
  <?php
$data = array(
        'name'          => 'body',
        'id'            => 'body',
        'class'            => 'form-control'

);
 echo form_textarea($data,set_value('body')); ?>

</div>
<div class="form-group col-md-6">
  <?php echo form_label('Image Upload', 'userfile'); ?>
<?php echo form_upload('userfile'); ?>
</div>

</div>

<div class="row">
  <div class="form-group col-md-3 text-right" style="margin-top:25px;">
  <?php
$atts = array('class'  => 'btn btn-default');
echo  anchor('admin/post','Cancel' ,$atts) ; ?>

<?php echo
form_submit(['value'=>'Add Post','class'=>'btn btn-success']); ?>
</div>
</div>

</div>
<?php echo form_close();?>
   </div>
           </div>
  </div>
