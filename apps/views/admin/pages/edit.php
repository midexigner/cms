<div class="col-md-10 content">
<div class="row">
 <div class="col-md-12">
<div class="panel panel-default">
     <div class="panel-heading text-center"><h4>Edit  New Post</h4></div>
     
     <div class="panel-body">
<?php echo validation_errors(); ?>
<?php 
$attributes = array('class' => 'form-', 'id' => 'myform');

echo form_open_multipart('admin/post/update',$attributes);
echo form_hidden('id',set_value('id',$post->id));
 ?>

<div class="form-group col-md-6">
  <?php $attributes = array(
        'class' => '',
);
echo form_label('Title: ', 'title', $attributes); ?>

  <?php
$data = array(
        'name'          => 'title',
        'id'            => 'title',
        'class'            => 'form-control',
        'maxlength'     => '60',
);
 echo form_input($data,set_value('title',$post->title)); ?>
  
</div>
<div class="form-group col-md-6">
  <?php $attributes = array(
        'class' => '',
);
echo form_label('Slug:', 'slug', $attributes); ?>

  <?php
$data = array(
        'name'          => 'slug',
        'id'            => 'slug',
        'class'            => 'form-control',
        'readonly'     => 'readonly'
);
 echo form_input($data,set_value('slug',$post->slug)); ?>
  
</div>
<div class="form-group col-md-8">
    <?php echo form_label('Content:', 'body'); ?>
  <?php
$data = array(
        'name'          => 'body',
        'id'            => 'body',
        'class'            => 'form-control'

);
 echo form_textarea($data,set_value('body',$post->body)); ?>

</div>

<div class="form-group col-md-4">
<label for="categories ">Categories </label>
<?php $parent_value =  $post->categories; ?>
<select class="form-control" name="categories" id="categories">
<option value="0"  <?php echo  (($parent_value == 0)?' selected="selected"':''); ?>>Please Select Category</option>
<?php foreach ($categories as $cat) { ?>
 <option value="<?php echo $cat['id'];?>" <?php echo  (($parent_value == $cat['id'])?' selected="selected"':''); ?>><?php echo $cat['name'];?></option>
<?php } ?>
  
</select>
</div>

<div class="row">
  <div class="form-group col-md-3 text-right" style="margin-top:25px;">
  <?php 
$atts = array('class'  => 'btn btn-default');
echo  anchor('admin/post','Cancel' ,$atts) ; ?>
  
<?php echo 
form_submit(['value'=>'Update Post','class'=>'btn btn-success']); ?>
</div>

</div>
</div>
<?php echo form_close();?>
   </div>  
           </div>           
  </div>



 
