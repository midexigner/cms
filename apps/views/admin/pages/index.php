<div class="col-md-10 content">
          <div class="row">
  <div class="col-md-12">
    <?php if($error = $this->session->flashdata('response')): ?>
  <?php echo $error; ?>
    <?php endif; ?>
  </div>

</div>
            <div class="row">
             <div class="col-md-12">

                <div class="panel panel-default">

                <div class="panel-heading text-center">

       	  <div class="row text-left">
<div class="col-md-6"><h4>Pages list</h4></div>
    <div class="col-md-6 text-right">
<?php $atts = array('class'  => 'btn btn-success btn-sm');
 echo  anchor('admin/pages/create','Add New Pages' ,$atts); ?>
    </div>
    </div>

	</div>
                 <div class="panel-body">

<table class="table table-bordered table-striped table-condensed">
<thead><th></th><th>Title</th><th>Slug</th><th>Content</th><th>Date</th></thead>
<tbody>
 <?php foreach ($posts as $post) {?>
     <tr>

    <td>
      <?php echo btn_edit("admin/post/edit/".$post['slug']); ?>
        <?php echo btn_delete("admin/post/delete/".$post['id']); ?>
    </td>
    <td><?php echo $post['title']; ?></td>
    <td><?php echo $post['slug']; ?></td>
    <td><?php echo character_limiter($post['body'],20); ?></td>
    <td><?php echo pretty_date($post['created_at']); ?></td>

  </tr>
 <?php } ?>

</tbody>
</table>


                 </div>

                </div>
                </div>

            </div>
