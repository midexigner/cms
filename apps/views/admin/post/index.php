<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    Posts
      <small><a href="<?php echo site_url('admin/post/create') ?>" class="btn btn-block btn-default">Add New</a></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Posts</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <?php if($error = $this->session->flashdata('response')): ?>
    <div class="row">
 <div class="col-md-12">

 <?php echo $error; ?>

 </div>
 </div>
  <?php endif; ?>
    <!-- Default box -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <!-- <h3 class="box-title">Title</h3> -->
      </div>
      <div class="box-body">



    <div class="nav-tabs-custom post-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#all" data-toggle="tab">All</a></li>
              <li><a href="#publish" data-toggle="tab">Publish</a></li>
              <li><a href="#trash" data-toggle="tab">Trash</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="all">
                <table  class="table table-bordered table-striped table-condensed table table-hover table-post">
            <thead><th></th><th>Title</th><th>Author</th><th>Categories</th><th>Tags</th><th>Date</th></thead>
            <tbody>
            <?php foreach ($posts as $post) {?>
             <tr>

            <td>
              <?php echo btn_edit("admin/post/edit/".$post['post_name']); ?>
                <?php echo btn_delete("admin/post/delete/".$post['id']); ?>
            </td>
            <td><?php echo $post['post_title']; ?></td>
            <td><?php echo $post['post_name']; ?></td>
            <td><?php echo character_limiter($post['post_content'],20); ?></td>
              <td>tag static</td>
            <td><?php echo pretty_date($post['created_at']); ?></td>

            </tr>
            <?php } ?>

            </tbody>
            </table>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="publish">
                <table  class="table table-bordered table-striped table-condensed table table-hover table-post">
            <thead><th></th><th>Title</th><th>Author</th><th>Categories</th><th>Tags</th><th>Date</th></thead>
            <tbody>
            <?php foreach ($posts as $post) {?>
             <tr>

            <td>
              <?php echo btn_edit("admin/post/edit/".$post['post_name']); ?>
                <?php echo btn_delete("admin/post/delete/".$post['id']); ?>
            </td>
            <td><?php echo $post['post_title']; ?></td>
            <td><?php echo $post['post_name']; ?></td>
            <td><?php echo character_limiter($post['post_content'],20); ?></td>
              <td>tag static</td>
            <td><?php echo pretty_date($post['created_at']); ?></td>

            </tr>
            <?php } ?>

            </tbody>
            </table>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="trash">
                <table  class="table table-bordered table-striped table-condensed table table-hover table-post">
            <thead><th></th><th>Title</th><th>Author</th><th>Categories</th><th>Tags</th><th>Date</th></thead>
            <tbody>
            <?php foreach ($posts as $post) {?>
             <tr>

            <td>
              <?php echo btn_edit("admin/post/edit/".$post['post_name']); ?>
                <?php echo btn_delete("admin/post/delete/".$post['id']); ?>
            </td>
            <td><?php echo $post['post_title']; ?></td>
            <td><?php echo $post['post_name']; ?></td>
            <td><?php echo character_limiter($post['post_content'],20); ?></td>
            <td>tag static</td>
            <td><?php echo pretty_date($post['created_at']); ?></td>

            </tr>
            <?php } ?>

            </tbody>
            </table>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->

      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        Footer
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
