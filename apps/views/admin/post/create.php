<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    Add Post
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Add Post</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <?php if($error = $this->session->flashdata('response')): ?>
    <div class="row">
 <div class="col-md-12">

 <?php echo $error; ?>

 </div>
 </div>
  <?php endif; ?>

    <!-- Default box -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body">
        <?php echo validation_errors(); ?>
        <?php
$attributes = array('class' => 'form-', 'id' => 'myform');
echo form_open_multipart('admin/post/create',$attributes); ?>
<div class="row">
<div class="form-group col-md-6">
  <?php $attributes = array(
        'class' => '',
);
echo form_label('Title: ', 'title', $attributes); ?>

  <?php
$data = array(
        'name'          => 'post_title',
        'id'            => 'post_title',
        'class'            => 'form-control',
        'maxlength'     => '60'
);
 echo form_input($data,set_value('title')); ?>
 <?php $data = array('name'=> 'post_name','id'=> 'post_name','class'=> 'form-control slug-static input-sm','readonly'=> 'readonly');
 echo form_input($data,set_value('post_name')); ?>
</div>

<div class="form-group col-md-6">
  <?php echo form_label('Featured', 'featured'); ?>
<?php echo form_upload('featured'); ?>
</div>


</div>

<div class="row">
  <div class="form-group col-md-6">
<label for="category ">Categories </label>
<select class="form-control" name="category" id="category">
<option value="0"  selected="selected">Please Select Category</option>
<?php foreach ($categories as $cat) { ?>
 <option value="<?php echo $cat['id'];?>"><?php echo $cat['name'];?></option>
<?php } ?>

</select>
</div>
<div class="form-group col-md-6">
<label for="tags ">Tags </label>
<select class="form-control" name="tags" id="tags">
<option value="0"  selected="selected">Please Select Tags</option>
<?php foreach ($categories as $cat) { ?>
<option value="<?php echo $cat['id'];?>"><?php echo $cat['name'];?></option>
<?php } ?>

</select>
</div>

</div>


<div class="row">
  <div class="form-group col-md-12">
      <?php echo form_label('Excerpt', 'body'); ?>
    <?php
  $data = array('name'=> 'post_excerpt','id'=> 'post_excerpt','rows' => '2','class'=> 'form-control');
   echo form_textarea($data,set_value('post_excerpt')); ?>
  </div>
</div>
<div class="row">
  <div class="form-group col-md-12">
      <?php echo form_label('&nbsp;', 'body'); ?>
    <?php
  $data = array('name'=> 'post_content','id'=> 'post_content','class'=> 'form-control editor');
   echo form_textarea($data,set_value('post_content')); ?>
  </div>
</div>

<div class="row">

  <div class="form-group col-md-12 text-center ">
  <?php
$atts = array('class'  => 'btn btn-default');
echo  anchor('admin/post','Cancel' ,$atts) ; ?>

<?php echo
form_submit(['value'=>'Publish','class'=>'btn btn-success']); ?>
</div>
</div>

<?php echo form_close();?>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->





  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
