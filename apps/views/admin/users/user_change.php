<div class="col-md-10 content">

<div class="row">
 <div class="col-md-12">
<div class="panel panel-default">
     <div class="panel-heading text-center">Change Password</div>
     
     <div class="panel-body">
<?php echo validation_errors(); ?>
<?php 
$attributes = array('class' => 'form-', 'id' => 'myform');
echo form_open('admin/users/change',$attributes); ?>
<div class="form-group col-md-12">
	<?php $attributes = array(
        'class' => '',
);
echo form_label('Old Password', 'old_password', $attributes); ?>

  <?php
$data = array(
        'name'          => 'old_password',
        'id'            => 'old_password',
        'class'            => 'form-control',
        'maxlength'     => '60'
);
 echo form_input($data,set_value('')); ?>
  
</div>

<div class="form-group col-md-12">
    <?php $attributes = array(
        'class' => '',
);
echo form_label('Password', 'password', $attributes); ?>
  <?php
$data = array(
        'name'          => 'password',
        'id'            => 'password',
        'class'            => 'form-control',
        'maxlength'     => '60'
);
 echo form_password($data,set_value('')); ?>
</div>
<div class="form-group col-md-12">
      <?php $attributes = array(
        'class' => '',
);
echo form_label('Confirm Password', 'confirm', $attributes); ?>
  <?php
$data = array(
        'name'          => 'confirm',
        'id'            => 'confirm',
        'class'            => 'form-control',
        'maxlength'     => '60'
);
 echo form_password($data,set_value('')); ?>
  
</div>

<div class="form-group col-md-12 text-right" style="margin-top:25px;">
	<?php 
$atts = array('class'  => 'btn btn-default');
echo  anchor('admin/users','Cancel' ,$atts) ; ?>
  
<?php echo 
form_submit(['value'=>'Change Password','class'=>'btn btn-success']); ?>
</div>

</div>
<?php echo form_close();?>
   </div>  
           </div>           
  </div>


