<div class="col-md-10 content">
          <div class="row">
  <div class="col-md-12">
    <?php if($error = $this->session->flashdata('response')): ?>
  <?php echo $error; ?>
    <?php endif; ?>
  </div>

</div>      
            <div class="row">
             <div class="col-md-12">
                
                <div class="panel panel-default">
          
                <div class="panel-heading text-center">
		
       	  <div class="row text-left">
<div class="col-md-6"><h4>Users list</h4></div>
    <div class="col-md-6 text-right">
<?php $atts = array('class'  => 'btn btn-success btn-sm');
 echo  anchor('admin/users/create','Add New User' ,$atts); ?>
    </div>
    </div>
		
	</div> 
                 <div class="panel-body">
 <table class="table table-bordered table-striped table-condensed">
<thead><th></th><th>Name</th><th>Email</th><th>Join Date</th><th>Last Login</th><th>Role</th></thead>
<tbody>
 <?php foreach ($users as $user) {?> 
     <tr>
     	
    <td>
      <?php echo btn_edit("admin/users/edit/".$user['id']); ?>
      <?php

      if($user['id'] != $this->session->userdata('id')):
      echo btn_delete("admin/users/delete/".$user['id']);
endif;


       ?>

    </td>
    <td><?php echo $user['first_name']; ?></td>
    <td><?php echo $user['email']; ?></td>
    <td><?php echo $user['join_date']; ?></td>
    <td><?php echo (($user['last_login'] == null)?"Never":pretty_date($user['last_login']).' , '. time_left($user['last_login'])); ?></td>
    <td> <?php echo $user['role'];?></td>
  </tr>
 <?php } ?> 

</tbody>
</table>              
 
                     
                 </div>
                 
                </div>
                </div>
            
            </div>
      