<div class="col-md-10 content">
<div class="row">
 <div class="col-md-12">
<div class="panel panel-default">
     <div class="panel-heading text-center">Add a New User</div>
     
     <div class="panel-body">
<?php echo validation_errors(); ?>
<?php 
$attributes = array('class' => 'form-', 'id' => 'myform');
echo form_open('admin/users/create',$attributes); ?>
<div class="form-group col-md-6">
	<?php $attributes = array(
        'class' => '',
);
echo form_label('First Name', 'full_name', $attributes); ?>

  <?php
$data = array(
        'name'          => 'first_name',
        'id'            => 'first_name',
        'class'            => 'form-control',
        'maxlength'     => '60'
);
 echo form_input($data,set_value('first_name')); ?>
  
</div>
<div class="form-group col-md-6">
  <?php $attributes = array(
        'class' => '',
);
echo form_label('Last Name', 'last_name', $attributes); ?>

  <?php
$data = array(
        'name'          => 'last_name',
        'id'            => 'last_name',
        'class'            => 'form-control',
        'maxlength'     => '60'
);
 echo form_input($data,set_value('last_name')); ?>
  
</div>
<div class="form-group col-md-6">
    <?php echo form_label('Email Address:', 'email'); ?>
  <?php
$data = array(
        'name'          => 'email',
        'id'            => 'email',
        'class'            => 'form-control',
        'maxlength'     => '60'
);
 echo form_input($data,set_value('email')); ?>

</div>
<div class="form-group col-md-6">
    <?php echo form_label('Status:', 'status'); ?>
  <select class="form-control" name="status">
  <option value="inactive">Inactive</option>
  <option value="active">Active</option>
</select>
</div>
<div class="form-group col-md-6">
    <?php echo form_label('Password', 'password'); ?>
  <?php
$data = array(
        'name'          => 'password',
        'id'            => 'password',
        'class'            => 'form-control',
        'maxlength'     => '60'
);
 echo form_password($data,set_value('password')); ?>
</div>
<div class="form-group col-md-6">
      <?php $attributes = array(
        'class' => '',
);
echo form_label('Confirm Password', 'confirm', $attributes); ?>
  <?php
$data = array(
        'name'          => 'confirm',
        'id'            => 'confirm',
        'class'            => 'form-control',
        'maxlength'     => '60'
);
 echo form_password($data,set_value('confirm')); ?>
  
</div>
<div class="form-group col-md-6">
     <?php echo form_label('Role:', 'role'); ?>
  <select class="form-control" name="role">
  <option value=""></option>
  <option value="editor">Editor</option>
  <option value="admin">Admin</option>
</select>
</div>
<div class="form-group col-md-6 text-right" style="margin-top:25px;">
	<?php 
$atts = array('class'  => 'btn btn-default');
echo  anchor('admin/users','Cancel' ,$atts) ; ?>
  
<?php echo 
form_submit(['value'=>'Add User','class'=>'btn btn-success']); ?>
</div>

</div>
<?php echo form_close();?>
   </div>  
           </div>           
  </div>



 
